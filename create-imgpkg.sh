#!/bin/bash
manifest_file=$1
app_name=$2
output="manifest_config"
registry=harbor.msu.tsfrt.net

push_imgpkg () {
 
  imgpkg push \
  -b $registry/${app_name}-imgpkg/${app_name}:${this_env} \
  -f $app_name/common \
  -f $app_name/$1 \
  -f $output/$1.yml 
}


copy_imgpkg () {

  imgpkg copy \
  -b $registry/${app_name}-imgpkg/${app_name}:${this_env} \
  --to-repo $registry/${app_name}-imgpkg-bundle/${app_name}
}

kbld_lock_file_for_env () {

  mkdir -p $app_name/$1/.imgpkg
  
  ytt -f $app_name/common \
  -f $app_name/$1 \
  -f $output/$1.yml \
  | kbld -f- \
  --imgpkg-lock-output $app_name/$1/.imgpkg/images.yml 
}

create_data_from_manifest () {
  env=$(echo $1 | awk -F"," '{print $1}' )
  tag=$(echo $1 | awk -F"," '{print $2}' )
  type=$(echo $1 | awk -F"," '{print $3}' )
  
  mkdir -p $output
  output_file=$output/${env}.yml
  echo "#@ load('"@ytt:overlay"', '"overlay"')" > $output_file
  echo "#@data/values" >> $output_file
  echo "---" >> $output_file
  echo "#@overlay/match missing_ok=True" >> $output_file
  echo "env: $env" >> $output_file
  echo "#@overlay/match missing_ok=True" >> $output_file
  echo "tag: $tag" >> $output_file
  echo "#@overlay/match missing_ok=True" >> $output_file
  echo "namespace: $type" >> $output_file

  echo $env
}

while read line; do
  this_env=$(create_data_from_manifest $line)
  kbld_lock_file_for_env $this_env
  push_imgpkg $this_env
  copy_imgpkg $this_env
done <$manifest_file


